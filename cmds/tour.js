const ora = require('ora');
const api = require('../utils/api')

module.exports = async (args) => {
  const spinner = ora().start();
  try {
    const location = args.location || args.l
    const wether = await api(location);

    spinner.stop()
    console.log(`Current conditions in : ${location}: `)
    console.log(`\t ${wether.condition.temp} S, ${wether.condition.text}`);
  } catch( err ){
    spinner.stop();
    console.error(err);
  }
}