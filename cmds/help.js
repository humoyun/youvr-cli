const menus = {
  main: `
    youvr [command] <options>

    tour .............. show weather for today
    version ............ show package version
    help ............... show help menu for a command
  `,

  tour: `
    youvr tour <options>

    --location, -l ..... the location to use
  `,

  scene: `
    youvr scene <options>

    --location, -l ..... the location to use
  `
}

module.exports = (args) => {
  const subCmd= args._[0] === 'help' ? args._[1] : args._[0];

  console.log(menus[subCmd] || menus.main);
}