const minimist = require('minimist')

module.exports = () => {
  console.log('Welcome to The YouVR V.2 CLI App!');

  const args = minimist(process.argv.slice(2));
  let cmd = args._[0] || 'help';

  if (args.version || args.v) {
    cmd = 'version'
  }
  if (args.help || args.h) {
    cmd = 'help'
  }

  switch(cmd) {
    case 'tour': 
      require('./cmds/tour')(args);
      break;

    case 'scene': 
      require('./cmds/scene')(args);
      break;

    case 'help': 
      require('./cmds/help')(args);
      break;

    case 'version': 
      require('./cmds/version')(args);
      break;
      
    default:
      console.error(`"${cmd}" is not a valid command!`)
  }  
}